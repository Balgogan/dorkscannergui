# importing libraries
from dorkscan import DorkScan
from PyQt5.QtWidgets import * 
import sys

 
# creating a class
# that inherits the QDialog class
class Window(QDialog):
    

    # get dork file path
    def dialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self,"QFileDialog.getOpenFileName()", "", "Text Files (*.txt)", options=options)
        if fileName:
            self.fileName = fileName

    def saveFile(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        outputFileName, _ = QFileDialog.getSaveFileName(self, "QFileDialog.getSaveFileName()", "", "Text Files(*.txt)", options=options)
        if outputFileName:
            self.outputFileName = outputFileName
            file = open(outputFileName,'w')
            file.write('URL List :\n')
            file.close()

 	
    # constructor
    def __init__(self):
        super(Window, self).__init__()

        # init variables
        self.outputFileName = None
        self.fileName = None
        self.pageNumbers = 0
        # setting window title
        self.setWindowTitle("Dorkscanner GUI")
  
        # setting geometry to the window
        self.setGeometry(500, 500, 500, 500)
  
        # creating a group box
        self.formGroupBox = QGroupBox("Powered By @balgogan and @unkwn1")
  
        # creating spin box to select age
        self.pageNumbers = QSpinBox()
  
        # creating combo box to select degree
        self.degreeComboBox = QComboBox()
  
        # adding items to the combo box
        self.degreeComboBox.addItems(["Bing", "Ask", "Wow"])
  
        # creating a push button for dorks file
        self.fileNameButton = QPushButton('Choose File', self)
       
        # creating a push button for output file
        self.outputFileNameButton = QPushButton('Choose File', self) 
        #self.lineEdit = QLineEdit()
        #self.lineEdit.setPlaceholderText("Example : output.txt")

        # call dialog() function and open the dialog window to open dorks file
        self.fileNameButton.clicked.connect(self.dialog)
        self.fileNameButton.move(50,50)
        
        # call saveFile() function and open the dialog window to save output file
        self.outputFileNameButton.clicked.connect(self.saveFile)
        self.outputFileNameButton.move(50,50)

        # calling the method that create the form
        self.createForm()
  
        # creating a dialog button for ok and cancel
        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
  
        # adding action when form is accepted
        self.buttonBox.accepted.connect(self.dorkScan)
  
        # adding action when form is rejected
        self.buttonBox.rejected.connect(self.reject)

        # creating a vertical layout
        mainLayout = QVBoxLayout()
  
        # adding form group box to the layout
        mainLayout.addWidget(self.formGroupBox)
  
        # adding button box to the layout
        mainLayout.addWidget(self.buttonBox)
  
        # setting lay out
        self.setLayout(mainLayout)
        
        # dork scan function called when form is accepted
    def dorkScan(self):
        '''File Name : /home/balgogan/Bureau/test.txt
           Engine : Bing
           Page Numbers : 0
           Output File : /home/balgogan/Bureau/text.txt
        '''
        #try:
        fileName = self.fileName
        fileName = open(fileName,'r').readlines()
        print(fileName)
        engine = self.degreeComboBox.currentText()
        dorks = int(self.pageNumbers.text())
        outputFileName = self.outputFileName

        results = DorkScan(fileName, engine, dorks, silent=False)
        try:
            for result in results:
                for dict in result.get('results'):
                    for url in dict.get('urls'):
                        with open(outputFileName,'a') as f:
                            f.write(f'{url}\n')

        except:
            error = QMessageBox()
            error.critical(self, "Dorkscanner GUI", "Error ! No results ( Try to increase page numbers or change the dork file )")
            self.close()
            sys.exit()
            


        #except:
        #error = QMessageBox()
        #error.critical(self, "Dorkscanner GUI", "Error ! Missing Parameters")
        self.close()

        # printing the form information
        try:
            print("File Name : {0}".format(self.fileName))
        except:
            self.fileName = "Error"
            print("File Name : {0}".format(self.fileName))

        try:
            print("Output File : {0}".format(self.outputFileName))
        except:
            self.outputFileName = "Error"
            print("File Name : {0}".format(self.outputFileName))

        print("Engine : {0}".format(self.degreeComboBox.currentText()))
        print("Page Numbers : {0}".format(self.pageNumbers.text()))
 
        # closing the window
        self.close()  
    

    # create form method
    def createForm(self):
  
        # creating a form layout
        layout = QFormLayout()
  
        # adding rows
        # for dork file and adding input text
        layout.addRow(QLabel("Dorks File:"), self.fileNameButton)
  
        # for engine and adding combo box
        layout.addRow(QLabel("Engine:"), self.degreeComboBox)
  
        # for pages number and adding spin box
        layout.addRow(QLabel("Pages Number:"), self.pageNumbers)

        # for output filename and adding output text
        layout.addRow(QLabel("Output Filename:"),self.outputFileNameButton)
        # setting layout
        self.formGroupBox.setLayout(layout)
  
  
# main method
if __name__ == '__main__':
  
    # create pyqt5 app
    app = QApplication(sys.argv)

    # create the instance of our Window
    window = Window()
    
    # showing the window
    window.show()
    # start the app
    sys.exit(app.exec())
